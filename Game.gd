# Copyright (c) 2018 Ilaria Cislahi, Thomas Herzog.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

export var duration = 45
export var clean_growth = 0.05
var clean_amount = 0

var hot_decay_time = 10
var hot_decay_amount = 0.3
var current_decay_amount = 0
var decay_tween = Tween.new()

export var toilet_modifier = 0.6

export var event_frequency_min = 5
export var event_frequency_max = 8

export var event_length_min = 3
export var event_length_max = 5

func _ready():
	randomize()
	$TemperatureControl.connect("flow_changed", $Shower, "set_shower_strength")
	add_toilet_usage()
	$TemperatureControl.connect("flow_changed", self, "_on_flow_changed")
	var hot_effectiveness_tween = Tween.new()
	var cold_effectiveness_tween = Tween.new()
	decay_tween.connect("tween_completed", self, "start_hot_decay")
	hot_effectiveness_tween.interpolate_property( $TemperatureControl,"hot_pipe_effectiveness", 0, 1, 10,Tween.TRANS_SINE,Tween.EASE_OUT_IN)
	cold_effectiveness_tween.interpolate_property( $TemperatureControl,"cold_effectiveness", 0, 1, 5,Tween.TRANS_SINE,Tween.EASE_OUT_IN)
	add_child(hot_effectiveness_tween)
	add_child(cold_effectiveness_tween)
	hot_effectiveness_tween.start()
	cold_effectiveness_tween.start()
	yield(hot_effectiveness_tween,"tween_completed")
	add_child(decay_tween)
	start_hot_decay(0,0)
	pass

func start_hot_decay(blub, blergh):
	decay_tween.interpolate_property( self,"current_decay_amount", 0, hot_decay_amount, hot_decay_time,Tween.TRANS_SINE,Tween.EASE_OUT_IN)
	decay_tween.start()



func _process(delta):
	$TemperatureControl.hot_decay = current_decay_amount
	clean_amount += clean_growth * delta * flow
	if(clean_amount >= 1):
		State.state = 2
		get_tree().change_scene("res://WinLossScreen.tscn")
		print("Win")

func add_toilet_usage():
	yield(get_tree().create_timer(randf()*(event_frequency_max-event_frequency_min) + event_frequency_min),"timeout")
	$ToiletBubble.appear()
	print("Someone is using the toilet")
	$TemperatureControl.cold_modifier -= toilet_modifier
	var t = get_tree().create_timer(randf()*(event_length_max-event_length_min) + event_length_min)
	t.connect("timeout", self, "remove_toilet_usage")

func remove_toilet_usage():
	$TemperatureControl.cold_modifier += toilet_modifier
	$ToiletBubble.disappear()
	add_toilet_usage()

var flow = 0
func _on_flow_changed(fl):
	flow = fl

func _on_Hooman_stress_changed(stress):
	if stress >= 1:
		if $Hooman.temp> 0:
			State.state = 3
			get_tree().change_scene("res://WinLossScreen.tscn")
		else:
			State.state = 0
			get_tree().change_scene("res://WinLossScreen.tscn")
	pass # replace with function body
