# Copyright (c) 2018 Ilaria Cislahi, Thomas Herzog.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

extends Control

var cold_amount = 0
var hot_amount = 0

signal temperature_changed(temperature)
signal flow_changed(flow)

var pipe_inertia = 0.02
var pipe_current_temperature = 0

var hot_modifier = 0
var cold_modifier = 0
var hot_decay = 0

var hot_pipe_effectiveness = 1
var cold_pipe_effectiveness = 1

func get_flow():
	return (hot_amount + cold_amount)/2

func get_temperature():
	#TODO
	return hot_amount - cold_amount
	
func _physics_process(delta):
	pipe_current_temperature = lerp(pipe_current_temperature, (hot_amount + hot_modifier - hot_decay)*hot_pipe_effectiveness - (cold_amount + cold_modifier)*cold_pipe_effectiveness , pipe_inertia)
	emit_signal("temperature_changed",pipe_current_temperature)
	$HBoxContainer/Flow.value = (pipe_current_temperature + 1)/2

func _on_WaterHandles_cold_changed(amount):
	cold_amount = clamp(cold_amount+ amount, 0,1)
	notify_observers()
	pass # replace with function body

func notify_observers():
#	emit_signal("temperature_changed",get_temperature())
	emit_signal("flow_changed", get_flow())
	$HBoxContainer/Flow.value = get_flow()
	$HBoxContainer/Temperature.value = get_temperature()

func _on_WaterHandles_hot_changed(amount):
	hot_amount = clamp(hot_amount+ amount, 0,1)
	notify_observers()
	pass # replace with function body
