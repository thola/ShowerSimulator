# Copyright (c) 2018 Ilaria Cislahi, Thomas Herzog.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

extends Node2D

#To you, that are looking to this code .. this is not the right way to do it
#Replicated code is a bad thing and i should have done a script and attach it to the handles
#but hey, it's a jam

signal hot_changed(amount)
signal cold_changed(amount)

export var handle_speed = 300
export var angular_handle_speed = PI/4

var move_cold = 0
var move_hot = 0

var hot_amount = 0
var cold_amount = 0

var was_hot_pressed = false
var was_cold_pressed = false

func _on_ColdHandleCollision_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton):
		was_cold_pressed = event.is_pressed()
		return
	if (event is InputEventMouseMotion) and was_cold_pressed:
		var am = sign(event.relative.dot(Vector2(0,-1)))*event.relative.length()/handle_speed
		cold_amount = clamp( cold_amount + am,0,1)
		$ColdHandleCollision/HandleCold.rotation = ((-PI)/2)*cold_amount
		emit_signal("cold_changed", am)
		 


func _on_HotHandleCollision_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton):
		was_hot_pressed = event.is_pressed()
		return
	if (event is InputEventMouseMotion) and was_hot_pressed:
		var am = -1*sign(event.relative.dot(Vector2(0,-1)))*event.relative.length()/handle_speed
		hot_amount = clamp( hot_amount + am,0,1)
		$HotHandleCollision/HandleHot.rotation = ((-PI)/2)*hot_amount
		emit_signal("hot_changed", am)



func _on_ColdHandleCollision_mouse_exited():
	was_cold_pressed = false
	pass # replace with function body


func _on_HotHandleCollision_mouse_exited():
	was_hot_pressed = false
	pass # replace with function body
