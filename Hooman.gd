# Copyright (c) 2018 Ilaria Cislahi, Thomas Herzog.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

extends Node2D

var clean_percentage = 0
var stress_level = 0
var temp = 0
var fl = 0

export var stress_growth_speed = 0.2
export var stress_decay_speed = 0.05

export var desired_temperature = 0.3
export var tolerance = 0.1

export (Color) var cold_color
export (Color) var hot_color
var normal_color = Color(1,1,1,1)

signal stress_changed(stress)

onready var animation_tree = $AnimationTreePlayer
export var washing_timeout = 2.0
var washing_timeout_timer

func _ready():
	connect("stress_changed", self, "_animation_handle_stress_changed")
	
	washing_timeout_timer = Timer.new()
	washing_timeout_timer.wait_time = washing_timeout
	washing_timeout_timer.connect("timeout", self, "_on_washing_timeout")
	add_child(washing_timeout_timer)
	washing_timeout_timer.start()

func _process(delta):
	var stress_changed = false
	if temp < desired_temperature - tolerance:
		#TODO make it grow proportionally
		stress_level += stress_growth_speed * delta * fl
		var alpha = ((desired_temperature - tolerance) - temp)/(abs(1-(desired_temperature+tolerance)))
		$Node2D.modulate = cold_color.linear_interpolate(normal_color, 1-alpha)
		stress_changed = true
	elif temp > desired_temperature + tolerance:
		stress_level += stress_growth_speed * delta *fl
		var alpha = (temp - (desired_temperature + tolerance))/(1-(desired_temperature+tolerance))
		$Node2D.modulate = hot_color.linear_interpolate(normal_color, 1-alpha)
		stress_changed = true
	else:
		stress_level = clamp (stress_level - stress_decay_speed * delta, 0,1)
		stress_changed = true
	if(stress_changed):
		emit_signal("stress_changed", stress_level)
	
	process_animation(delta)


func _on_temperature_changed(temperature):
	temp = temperature

func _on_flow_changed(flow):
	fl = flow



#
# Animation
#
func process_animation(delta):
	# TODO
	pass

func _on_washing_timeout():
	var next_state = randi() % 2
	animation_tree.transition_node_set_current("WashingTransition", next_state)

func _animation_handle_stress_changed(stress):
	var stress_state = 0
	
	if stress >= 0.65:
		stress_state = 1
	
	animation_tree.transition_node_set_current("TooHotTransition", stress_state)

func smoothstep(edge0, edge1, x):
    var t = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0)
    return t * t * (3.0 - 2.0 * t)